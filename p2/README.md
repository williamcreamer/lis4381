# LIS 4381 - Mobile Web Application Development

## William Creamer

### Project 2 Requirements:

*Five parts:*

1. Modify the web form from [Assignment 5](https://bitbucket.org/williamcreamer/lis4381/src/master/a5 "Assignment 5") to:
    * a) Have a working "Edit" button which allows the user to edit data for records within the "petstore" table.
    * b) Validate the edited data through server-side validation.
    * c) Have a working "Delete" button which allows the user to delete records within the "petstore" table.
2. Develop a web page that successfully displays a RSS feed.
3. Questions
4. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381
5. Localhost Link
    * [http://localhost/repos/lis4381/](http://localhost/repos/lis4381/ "Localhost Link")

#### README.md file should include the following items:

* Screenshot before successful edit of data
* Screenshot after successful edit of data
* Screenshot of failed validation
* Screenshot before successful deletion of data
* Screenshot after successful deletion of data
* Screenshot of RSS feed

#### Assignment Screenshots:

|*Screenshot Before Edit*|*Screenshot of Proposed Edit*|
|:---:|:---:|
| ![Pre-edit screenshot](img/beforeEdit.png) | ![Proposed edit screenshot](img/duringEdit.png) |

|*Screenshot of Failed Validation*|*Screenshot of Passed Validation*|
|:---:|:---:|
| ![Valid data screenshot](img/failedValidation.png) | ![Passed validation screenshot](img/passedValidation.png) |

|*Screenshot of Delete Prompt*|*Screenshot of Successful Deletion*|
|:---:|:---:|
| ![Delete prompt screenshot](img/deletePrompt.png) | ![Successful Deletion screenshot](img/successfulDelete.png) |

***Screenshot of RSS Feed:***

![RSS Feed screenshot](img/rssFeed.png)
#### Related Links:

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")
* Link to localhost:
[Localhost Link](http://localhost/repos/lis4381/ "Localhost Link")