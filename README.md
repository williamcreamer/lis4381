# LIS 4381 - Mobile Web Application Development

## William Creamer

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Skill Set 1: Even Or Odd program
    - Skill Set 2: Largest Number program
    - Skill Set 3: Arrays and Loops program
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Develop an application that calculates concert ticket prices
    - Provide screenshots of completed app
    - Develop a pet store database
    - Provide screenshots of ERD
    - Skill Set 4: Decision Structures program
    - Skill Set 5: Psuedo-Random Number Generator program
    - Skill Set 6: Methods program
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Develop a web application that:
        * a) Displays all work currently completed in LIS 4381 with clickable links and working navigation.
        * b) Has a functioning carousel rotating through three images with clickable links.
        * c) Contains a web form using client-side validation and regexp expressions.
    - Skill Set 10: Array List
    - Skill Set 11: Alpha/Numeric/Special Character Determiner
    - Skill Set 12: Temperature Conversion
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Develop a web form that:
        * a) Displays all petstores within the petstore table.
        * b) Has a working "Add Pet Store" button which allows the user to enter valid data for additional pet stores, validated through server-side validation.
        * c) Has non-working "Edit" and "Delete" buttons for each record in the table (to be completed in [Project 2](https://bitbucket.org/williamcreamer/lis4381/src/master/p2 "Project 2")).
    - Skill Set 13: Sphere Volume Calculator
    - Skill Set 14: Simple Calculator Web Application
    - Skill Set 15: Write/Read File Web Application    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Develop a business card application that:
        * a) Displays a launcher icon image in both activities (screens).
        * b) Adds background color(s) to both activities.
        * c) Adds a border around the image and button.
        * d) Adds text shadow (button).
    - Skill Set 7: Pseudo-Random Number Generator Data Validation
    - Skill Set 8: Largest of Three Numbers
    - Skill Set 9: Array Runtime Data Validation
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify the web form from [Assignment 5](https://bitbucket.org/williamcreamer/lis4381/src/master/a5 "Assignment 5") to:
        * a) Have a working "Edit" button which allows the user to edit data for records within the "petstore" table.
        * b) Validate the edited data through server-side validation.
        * c) Have a working "Delete" button which allows the user to delete records within the "petstore" table.
    - Develop a web page that successfully displays a RSS feed.