# LIS 4381 - Mobile Web Application Development

## William Creamer

### Assignment 4 Requirements:

*Six parts:*

1. Develop a web application that:
    * a) Displays all work currently completed in LIS 4381 with clickable links and working navigation.
    * b) Has a functioning carousel rotating through three images with clickable links.
    * c) Contains a web form using client-side validation and regexp expressions.
2. Skill Set 10: Array List
    * Skill Set 10 link: [Skill Set 10](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/10_ArrayList "Skill Set 10")
3. Skill Set 11: Alpha/Numeric/Special Character Determiner
    * Skill Set 11 link: [Skill Set 11](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/11_AlphaNumericSpecial "Skill Set 11")
4. Skill Set 12: Temperature Conversion
    * Skill Set 12 link: [Skill Set 12](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/12_TemperatureConversion "Skill Set 12")
5. Questions
6. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381

#### README.md file should include the following items:

* Screenshot of LIS 4381 Online Portfolio web application running
* Screenshot of Pet Store form validation
* Screenshot of Array List program running
* Screenshot of Alpha/Numeric/Special Character Determiner program running
* Screenshot of Temperature Conversion program running

#### Assignment Screenshots:

***Screenshot of LIS 4381 Online Portfolio***
![LIS 4381 Online Portfolio screenshot](img/onlinePortfolio.png) 

|*Screenshot of Pet Store form validation (1/2)*|*Screenshot of Pet Store form validation (2/2)*|
|:---:|:---:|
| ![Pet Store form validation screenshot 1](img/formValidation1.png) | ![Pet Store form validation screenshot 2](img/formValidation2.png) |

***Screenshot of Array List running***
![Array List program screenshot](img/arrayList.png)

|*Screenshot of Alpha/Numeric/Special Character Determiner program running*|*Screenshot of Temperature Conversion program running*|
|:---:|:---:|
| ![Alpha/Numeric/Special Character Determiner program screenshot](img/alphaNumericSpecial.png) | ![Temperature Conversion program screenshot](img/tempConversion.png) |

#### Related Links:

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")