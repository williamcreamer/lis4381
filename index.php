<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="William Creamer">
		<link rel="icon" href="favicon.ico">

		<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		 h2
		 {
			 margin: 0;     
			 color: #7f3741;
			 padding-top: 0px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif;    
			 text-shadow: 3px 3px #d9bba3;
		 }
		 h3
		 {	
			margin: 0;
			color: #ffffff;
			padding-top: 0px;
			font-size: 32px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 0px 0px 4px #000000, 0px 0px 4px #7f3741;
		 }
		 p
		 {	
			background-color: hsla(352, 40%, 36%, 0.75);
			border-radius: 6px;
			margin: 0;
			color: #efccb0;
			padding-top: 0px;
			font-size: 14px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 0px 0px 1px #efccb0;
		 }
		 .item
		 {
			 border: 1px solid black;
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="3000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/bitbucket.png) no-repeat left center; background-size: cover;" alt="Mobile Web Applications Development Link">
		 						<a href="https://bitbucket.org/williamcreamer/lis4381/">
								<div class="container">
									<div class="carousel-caption">
										<h3>LIS 4381: Mobile Web Application Development</h3>									
										<p>I developed multiple mobile web applications and explored various concepts using Java, PHP, and XML.</p>
                        			</div>
                      			</div>
								</a>
                    		</div>
							<div class="item" style="background: url(img/businessCardApp.png) no-repeat left center; background-size: cover;" alt="Business Card Application Link">
		 						<a href="https://bitbucket.org/williamcreamer/lis4381/src/master/p1/">
								<div class="container">
									<div class="carousel-caption">
										<h3>Project 1: Business Card Application</h3>
										<p>I developed a business card application that has an image and button with borders around each, a launcher icon, text shadow, and unique background colors on both pages.</p>
									</div>
                      			</div>
								</a>
                    		</div>
							<div class="item" style="background: url(img/concertTicketApp.png) no-repeat left center; background-size: cover;" alt="Concert Ticket Application Link">
		 						<a href="https://bitbucket.org/williamcreamer/lis4381/">
								<div class="container">
									<div class="carousel-caption">
										<h3>Assignment 3: Concert Ticket Calculator Program</h3>
										<p>I developed an application with a launcher icon that prompts the user to select an artist from a drop-down menu and input the number of desired tickets, then displays the cost to the user.</p>
									</div>
                      			</div>
								</a>
                    		</div>							
							
						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
