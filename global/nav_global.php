<nav class="navbar navbar-dark navbar-fixed-top" style="background-color: #7f3741;">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php" target="_self" style="color:#d9bba3">LIS 4381</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<!-- <li class="active"><a href="index.php">LIS 4381</a></li> -->
					<li><a href="a1/index.php" style="color:#d9bba3">A1</a></li>
					<li><a href="a2/index.php" style="color:#d9bba3">A2</a></li>
					<li><a href="a3/index.php" style="color:#d9bba3">A3</a></li>
					<li><a href="a4/index.php" style="color:#d9bba3">A4</a></li>
					<li><a href="a5/index.php" style="color:#d9bba3">A5</a></li>
					<li><a href="p1/index.php" style="color:#d9bba3">P1</a></li>
					<li><a href="p2/index.php" style="color:#d9bba3">P2</a></li>
					<li><a href="skillsets/index.php" style="color:#d9bba3">Skill Sets</a></li>		
					<li><a href="test/index.php" style="color:#d9bba3">Test</a></li>	
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	
