<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 3 web page.">
		<meta name="author" content="William Creamer">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>	
		
		<style type="text/css">
		h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: </strong>Developed an application with a launcher icon that prompts the user to select an artist from a drop-down menu and input the number of desired tickets, then displaying the cost to the user. Developed a pet store database with three tables of ten unique records each. Completed Skill Sets 4, 5, and 6.
				</p>

				<h4>Concert Ticket Calculator (1/2)</h4>
				<img src="img/ticketApp1.png" class="img-responsive center-block" alt="Concert Ticket Calculator (1/2)">

				<h4>Concert Ticket Calculator (2/2)</h4>
				<img src="img/ticketApp2.png" class="img-responsive center-block" alt="Concert Ticket Calculator (2/2)">

				<h4>Pet Store Database ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="Pet Store Database ERD">
				
				<h4>Pet Store Table</h4>
				<img src="img/petstore.png" class="img-responsive center-block" alt="Pet Store Table">
				
				<h4>Pet Table</h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="Pet Table">
				
				<h4>Customer Table</h4>
				<img src="img/customer.png" class="img-responsive center-block" alt="Customer Table">
				
				<h4>Skill Set 4: Decision Sructures Program (1/2)</h4>
				<img src="img/decisionStructures1.png" class="img-responsive center-block" alt="Decision Sructures Program (1/2)">
				
				<h4>Skill Set 4: Decision Sructures Program (2/2)</h4>
				<img src="img/decisionStructures2.png" class="img-responsive center-block" alt="Decision Sructures Program (2/2)">
				
				<h4>Skill Set 5: Psuedo-Random Number Generator Program</h4>
				<img src="img/psuedoRandomNumberGenerator.png" class="img-responsive center-block" alt="Psuedo-Random Number Generator Program">

				<h4>Skill Set 6: Methods Program</h4>
				<img src="img/methods.png" class="img-responsive center-block" alt="Methods sProgram">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
