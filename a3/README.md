# LIS 4381 - Mobile Web Application Development

## William Creamer

### Assignment 3 Requirements:

*Seven parts:*

1. Develop an application that:
    * a) Displays a launcher icon.
    * b) Prompts the user to select an artist and input the number of tickets.
    * c) Displays the cost to the user.
2. Develop a [database](https://bitbucket.org/williamcreamer/lis4381/src/master/a3/docs "Petstore Database") that:
    * a) Has three tables.
    * b) Has at least 10 records per table.
    * c) Displays the relationships and both the primary and foreign keys.    
2. Skill Set 4: Decision Structures
    * Skill Set 4 link: [Skill Set 4](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/4_DecisionStructures "Skill Set 4")
3. Skill Set 5: Psuedo-Random Number Generator
    * Skill Set 5 link: [Skill Set 5](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/5_PsuedoRandomNumberGenerator "Skill Set 5")
4. Skill Set 6: Methods
    * Skill Set 6 link: [Skill Set 6](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/6_Methods "Skill Set 6")
5. Questions
6. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381

#### README.md file should include the following items:

* Screenshots of the Concert Ticket Calculator application running
* Screenshots of ERD and select statements
* Screenshot of Decision Structures program running
* Screenshot of Psuedo-Random Number Generator program running
* Screenshot of Methods program running

#### Assignment Screenshots:

|*Screenshot of Concert Ticket Calculator running (1/2)*|*Screenshot of Concert Ticket Calculator running (2/2)*|
|:---:|:---:|
| ![Concert Ticket Calculator screenshot 1](img/ticketApp1.png) | ![Concert Ticket Calculator screenshot 2](img/ticketApp2.png) |

***Screenshot of ERD***:

![ERD screenshot](img/erd.png)

***Screenshot of table "petstore"***:

![petstore table screenshot](img/petstore.png)

****Screenshot of table "pet"***:

![pet table screenshot](img/pet.png)

***Screenshot* of table "customer"***:

![customer table screenshot](img/customer.png)

|*Screenshot of Decision Structures running (1/2)*|*Screenshot of Decision Structures running (2/2)*|
|:---:|:---:|
| ![Decision Structures program screenshot](img/decisionStructures1.png) | ![Decision Structures program screenshot 2](img/decisionStructures2.png) |

|*Screenshot of Psuedo-Random Number Generator program running*|*Screenshot of Methods program running*|
|:---:|:---:|
| ![Psuedo-Random Number Generator program screenshot](img/psuedoRandomNumberGenerator.png) | ![Methods program screenshot](img/methods.png) |

#### Petstore Database files:

* [a3.mwb](https://bitbucket.org/williamcreamer/lis4381/src/master/a3/docs/a3.mwb "a3.mwb file")
* [a3.sql](https://bitbucket.org/williamcreamer/lis4381/src/master/a3/docs/a3.sql "a3.sql")

#### Related Links:

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")