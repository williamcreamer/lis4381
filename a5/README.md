# LIS 4381 - Mobile Web Application Development

## William Creamer

### Assignment 5 Requirements:

*Seven parts:*

1. Develop a web form that:
    * a) Displays all petstores within the petstore table.
    * b) Has a working "Add Pet Store" button which allows the user to enter valid data for additional pet stores, validated through server-side validation.
    * c) Has non-working "Edit" and "Delete" buttons for each record in the table (to be completed in [Project 2](https://bitbucket.org/williamcreamer/lis4381/src/master/p2 "Project 2")).
2. Skill Set 13: Sphere Volume Calculator
    * Skill Set 13 link: [Skill Set 13](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/13_SphereVolumeCalculator "Skill Set 13")
3. Skill Set 14: Simple Calculator Web Application
    * Skill Set 14 link: [Skill Set 14](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/14_SimpleCalculator "Skill Set 14")
4. Skill Set 15: Write/Read File Web Application
    * Skill Set 15 link: [Skill Set 15](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/15_WriteReadFile "Skill Set 15")
5. Questions
6. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381
7. Localhost Link
    * [http://localhost/repos/lis4381/](http://localhost/repos/lis4381/ "Localhost Link")

#### README.md file should include the following items:

* Screenshot of LIS 4381 Online Portfolio web application running
* Screenshot of server-side form validation
* Screenshot of Sphere Volume Calculator program running
* Screenshot of Simple Calculator web application running
* Screenshot of Write/Read File web application running

#### Assignment Screenshots:

|*Screenshot of Invalid Data*|*Screenshot of Failed Validation*|
|:---:|:---:|
| ![Invalid data screenshot](img/invalid.png) | ![Failed validation screenshot](img/failedValidation.png) |

|*Screenshot of Valid Data*|*Screenshot of Passed Validation*|
|:---:|:---:|
| ![Valid data screenshot](img/valid.png) | ![Passed validation screenshot](img/passedValidation.png) |

***Screenshot of Sphere Volume Calculator running:***

![Sphere Volume Calculator program screenshot](img/sphereVolumeCalculator.png)

|*Screenshot of Simple Calculator addition (1/2)*|*Screenshot of Simple Calculator addition (2/2)*|
|:---:|:---:|
| ![Addition screenshot 1](img/simpleAddition1.png) | ![Addition screenshot 2](img/simpleAddition2.png) |

|*Screenshot of Simple Calculator division (1/2)*|*Screenshot of Simple Calculator division (2/2)*|
|:---:|:---:|
| ![Division screenshot 1](img/simpleDivision1.png) | ![Division screenshot 2](img/simpleDivision2.png) |

|*Screenshot of Write/Read File web application (1/2)*|*Screenshot of Write/Read File web application (2/2)*|
|:---:|:---:|
| ![Write/Read File web application screenshot 1](img/writeRead1.png) | ![Write/Read File web application screenshot 2](img/writeRead2.png) |

#### Related Links:

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")
* Link to localhost:
[Localhost Link](http://localhost/repos/lis4381/ "Localhost Link")