# LIS 4381 - Mobile Web Application Development

## William Creamer

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation; 
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App;
* git commands w/ short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations);

#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git fetch - Download objects and refs from another repository

#### Assignment Screenshots:

***Screenshot of AMPPS running http://localhost:***

![AMPPS Installation Screenshot](img/ampps.png)

***Screenshot of running java Hello:***

![JDK Installation Screenshot](img/jdk_install.png)

***Screenshot of Android Studio - My First App:***

![Android Studio Installation Screenshot](img/android.png)

#### Related Links:

* Bitbucket Tutorial - Station Locations:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/williamcreamer/bitbucketstationlocations/ "Bitbucket Station Locations")

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")