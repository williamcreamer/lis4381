<!DOCTYPE html>
<html lang="en">
  	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 1 web page.">
		<meta name="author" content="William Creamer">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>	
		
		<style type="text/css">
		h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		</style>
  	</head>

	<body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: </strong>Created a Bitbucket repository with distributed version control, installed Ampps and ran a test page within a browser, and installed Android Studio and created a beginner app.
				</p>

				<h4>Java Installation</h4>
				<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/android.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
