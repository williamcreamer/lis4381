<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Skill Sets web page.">
		<meta name="author" content="William Creamer">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Skill Sets</title>		
		<?php include_once("../css/include_css.php"); ?>	
		
		<style type="text/css">
		h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: </strong>Skill Sets 1-15.
				</p>

				<h4>Skill Set 1: Even or Odd Program</h4>
				<img src="1_EvenOrOdd/evenOrOdd.png" class="img-responsive center-block" alt="Even or Odd Program">
				
				<h4>Skill Set 2: Largest Number Program</h4>
				<img src="2_LargestNumber/largestNumber.png" class="img-responsive center-block" alt="Largest Number Program">

				<h4>Skill Set 3: Arrays and Loops Program</h4>
				<img src="3_ArraysAndLoops/arraysAndLoops.png" class="img-responsive center-block" alt="Arrays and Loops Program">

				<h4>Skill Set 4: Decision Sructures Program (1/2)</h4>
				<img src="4_DecisionStructures/decisionStructures1.png" class="img-responsive center-block" alt="Decision Sructures Program (1/2)">
				
				<h4>Skill Set 4: Decision Sructures Program (2/2)</h4>
				<img src="4_DecisionStructures/decisionStructures2.png" class="img-responsive center-block" alt="Decision Sructures Program (2/2)">
				
				<h4>Skill Set 5: Psuedo-Random Number Generator Program</h4>
				<img src="5_PsuedoRandomNumberGenerator/psuedoRandomNumberGenerator.png" class="img-responsive center-block" alt="Psuedo-Random Number Generator Program">

				<h4>Skill Set 6: Methods Program</h4>
				<img src="6_Methods/methods.png" class="img-responsive center-block" alt="Methods sProgram">

				<h4>Skill Set 7: Psuedo-Random Number Generator Data Validation Program</h4>
				<img src="7_PsuedoRandomNumberGeneratorDataValidation/psuedoRandomNumberGeneratorDataValidation.png" class="img-responsive center-block" alt="Psuedo-Random Number Generator Data Validation Program">
				
				<h4>Skill Set 8: Largest of Three Numbers Program</h4>
				<img src="8_LargestOfThreeNumbers/largestOfThreeNumbers.png" class="img-responsive center-block" alt="Largest of Three Numbers Program">

				<h4>Skill Set 9: Array Runtime Data Validation Program</h4>
				<img src="9_ArrayRuntimeDataValidation/arrayRuntimeDataValidation.png" class="img-responsive center-block" alt="Array Runtime Data Validation Program">

				<h4>Skill Set 10: Array List Program</h4>
				<img src="10_ArrayList/arrayList.png" class="img-responsive center-block" alt="Array List Program">

				<h4>Skill Set 11: Alpha/Numeric/Special Program</h4>
				<img src="11_AlphaNumericSpecial/alphaNumericSpecial.png" class="img-responsive center-block" alt="Alpha/Numeric/Special Program">

				<h4>Skill Set 12: Temperature Conversion Program</h4>
				<img src="12_TemperatureConversion/tempConversion.png" class="img-responsive center-block" alt="Temperature Conversion Program">

				<h4>Skill Set 13: Sphere Volume Calculator Program</h4>
				<img src="13_SphereVolumeCalculator/sphereVolumeCalculator.png" class="img-responsive center-block" alt="Sphere Volume Calculator Program">

				<h4><a href="14_SimpleCalculator/index.php">Skill Set 14: Simple Calculator Web Application</a></h4>
				<img src="14_SimpleCalculator/simpleAddition1.png" class="img-responsive center-block" alt="Simple Calculator Web Application">
				<img src="14_SimpleCalculator/simpleAddition2.png" class="img-responsive center-block" alt="Simple Calculator Web Application">
				<img src="14_SimpleCalculator/simpleDivision1.png" class="img-responsive center-block" alt="Simple Calculator Web Application">
				<img src="14_SimpleCalculator/simpleDivision2.png" class="img-responsive center-block" alt="Simple Calculator Web Application">

				<h4><a href="15_WriteReadFile/index.php">Skill Set 15: Write/Read Web Application</h4>
				<img src="15_WriteReadFile/writeRead1.png" class="img-responsive center-block" alt="Write/Read Web Application">
				<img src="15_WriteReadFile/writeRead2.png" class="img-responsive center-block" alt="Write/Read Web Application">

				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
