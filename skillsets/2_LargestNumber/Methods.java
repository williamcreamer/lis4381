import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        //Program introduction
        System.out.println("Developer: William Creamer");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
        System.out.println();
    }
    public static void largestNumber()
    {
        Scanner sc = new Scanner(System.in);

        //Declare variables
        int num1, num2, largestNum;
        
        //Prompt user for input & store
        System.out.print("Enter first integer: ");
        num1 = sc.nextInt();
        System.out.print("Enter second integer: ");
        num2 = sc.nextInt();

        //Determine if odd or even
        if (num1 > num2) {
            System.out.print(num1 + " is larger than " + num2 + ".");
        } else if (num2 > num1){
            System.out.print(num2 + " is larger than " + num1 + ".");
        } else if (num1 == num2) {
            System.out.print("Integers are equal.");
        }  
    }
}