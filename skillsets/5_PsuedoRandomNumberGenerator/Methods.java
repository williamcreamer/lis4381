import java.util.Scanner;
import java.util.Random; 

public class Methods
{
    public static void getRequirements() 
    {
        //Program introduction
        System.out.println("Developer: William Creamer\n");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
    }
    
    public static int[] createArray()
    {
        Scanner sc = new Scanner(System.in);

        int arraySize = 0;

        //prompt user for number of randomly generated numbers
        System.out.print("\nEnter desired number of pseudorandom-generated integers (min 1): ");
        arraySize = sc.nextInt();
        
        int yourArray[] = new int[arraySize];
        return yourArray;
    }
    
    public static void psuedoRandNumGen(int [] myArray)
    {
        Random r = new Random();
        int i = 0;
        //for loop
        System.out.println("\nfor loop:");
        for (i = 0; i < myArray.length; i++)
        {
            System.out.println(r.nextInt());
        }

        //enchanced for loop
        System.out.println("\nenhanced for loop:");
        for (int n: myArray)
        {
            System.out.println(r.nextInt());
        }
        i = 0;

        //while loop
        System.out.println("\nwhile loop:");
        while (i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }
        i = 0;

        //do...while loop
        System.out.println("\ndo...while loop:");
        do {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
    }
}