import java.util.Scanner;

class Methods
{
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements()
    {
        //Program introduction
        System.out.println("Developer: William Creamer");
        System.out.println("Temperature Conversion Program\n");
        System.out.println("Program converts user-entered temperatures into Farenheit or Celsius scales.");
        System.out.println("Program continues to prompt for user entry until no longer requested.");
        System.out.println("Note: Upper or lower case letters permitted. Incorrect entires are not.");
        System.out.println("Note: Program does not validate numeric data (optional requirement).");
        System.out.println();
    }

    public static void convertTemp() 
    {
        Scanner sc = new Scanner(System.in);
        double temperature = 0.0;
        char choice;
        char type;

        do
        {
            System.out.print("Farenheit to Celsius? Type \"f\", or Celsius to Farenheit? Type \"c\": ");
            type = sc.next().charAt(0);
            type = Character.toLowerCase(type);
            if (type == 'f')
            {
                System.out.print("Enter temperature in Farenheit: ");
                temperature = sc.nextDouble();
                temperature = ((temperature - 32) * 5) / 9;
                System.out.println("Temperature in Celsius = " + temperature);
            }
            else if (type == 'c')
            {
                System.out.print("Enter temperature in Celsius: ");
                temperature = sc.nextDouble();
                temperature = (temperature * 9/5) + 32;
                System.out.println("Temperature in Farenheit = " + temperature);
            }
            else
            {
                System.out.println("Incorrect entry. Please try again.");
            }

            System.out.print("\nDo yo want to convert a temperature (y or n)? ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        System.out.println("\nThank you for using the Temperature Conversion Program!");
    }
}