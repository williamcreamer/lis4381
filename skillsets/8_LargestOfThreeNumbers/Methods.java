import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        //Program introduction
        System.out.println("Developer: William Creamer");
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for non-numeric characters and non-integer values.");
        System.out.println();
    }
    public static void validateData()
    {
        Scanner sc = new Scanner(System.in);

        //Declare variables
        int num1, num2, num3, largestNum;
        
        //Prompt user for input & store
        System.out.print("Please enter first number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter first number: ");
            }
        num1 = sc.nextInt();
        System.out.print("Please enter second number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter second number: ");
            }
        num2 = sc.nextInt();
        System.out.print("Please enter third number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter third number: ");
            }
        num3 = sc.nextInt();

        largestNumber(num1, num2, num3);
    }

    public static void largestNumber(int num1, int num2, int num3)
    {
        System.out.println("\nYou entered: " + num1 + ", " + num2 + ", " + num3);

        if (num1 > num2 && num1 > num3) {
            System.out.println(num1 + " is largest.");
        } else if (num2 > num1 && num2 > num3) {
            System.out.println(num2 + " is largest.");
        } else if (num3 > num1 && num3 > num2) {
            System.out.println(num3 + " is largest.");
        } else {
            System.out.println("Integers are equal.");
        }       
    }
}