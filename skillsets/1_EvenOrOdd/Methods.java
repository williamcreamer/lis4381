import java.util.Scanner;

public class Methods
{
   public static void getRequirements()
   {
       //Program introduction
       System.out.print("Developer: William Creamer\n");
       System.out.print("Program evaluates integers as even or odd.\nNote: Program does *not* check for non-numeric characters\n\n");
   }

   public static void evaluateNumber()
   {
      Scanner sc = new Scanner(System.in);

      //declare variables
      int choice;
      double remainder;

      //Prompt user for input & store
      System.out.print("Enter integer: ");
      choice = sc.nextInt();

      //Determine if odd or even
      remainder = choice % 2;

      //Output results
      System.out.print(choice + " is an ");

      if (remainder != 0) {
          System.out.print("odd ");
      } else {
          System.out.print("even ");
      }

      System.out.print("number.\n");    
   }
}