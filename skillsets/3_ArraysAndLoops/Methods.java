import java.util.Scanner;

public class Methods
{
    public static void getRequirements() 
    {
        //Program introduction
        System.out.println("Developer: William Creamer\n");
        System.out.println("Program loops through an array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
    }
    public static void arraysAndLoops()
    {
        Scanner sc = new Scanner(System.in);

        //declare variables
        String [] strings = {"dog", "cat", "bird", "fish", "insect"};
        int i = 0;

        //for loop
        System.out.println("\nfor loop:");
        for (i = 0; i < strings.length; i++)
        {
            System.out.println(strings[i]);
        }

        //enchanced for loop
        System.out.println("\nenhanced for loop:");
        for (String test: strings)
        {
            System.out.println(test);
        }
        i = 0;

        //while loop
        System.out.println("\nwhile loop:");
        while (i < strings.length)
        {
            System.out.println(strings[i]);
            i++;
        }
        i = 0;

        //do...while loop
        System.out.println("\ndo...while loop:");
        do {
            System.out.println(strings[i]);
            i++;
        } while (i < strings.length);
    }
}