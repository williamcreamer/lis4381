import java.util.Scanner;

class Methods
{
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements()
    {
        //Program introduction
        System.out.println("Developer: William Creamer");
        System.out.println("Sphere Volume Program\n");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.");
        System.out.println("Must use Java's *built-in* PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non-numeric values.");
        System.out.println("Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.");
        System.out.println();
    }

    public static void getSphereVolume() 
    {
        int diameter = 0;
        double gallons = 0.0;
        double volume = 0.0;
        char choice = ' ';
        Scanner sc = new Scanner(System.in);

        do
        {
            System.out.print("Please enter diameter in inches: ");
            while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter diameter in inches: ");
            }
            diameter = sc.nextInt();

            System.out.println();

            volume = ((4.0/3.0) * Math.PI * Math.pow(diameter/2.0, 3));
            gallons = volume/231;
            System.out.println("Sphere volume: " + String.format("%,.2f", gallons) + " U.S. liquid gallons");
            

            System.out.print("\nDo yo want to calculate another sphere volume (y or n)? ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        System.out.println("\nThank you for using the Sphere Volume Calculator!");
    }
}