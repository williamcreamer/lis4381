import java.util.Scanner;

public class Methods
{
    public static void getRequirements() 
    {
        //Program introduction
        System.out.println("Developer: William Creamer\n");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following decision structures: if...else, and switch.");
        System.out.println("\nPhone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
    }
    
    public static void userPhoneType()
    {
        Scanner sc = new Scanner(System.in);

        //prompt user for number of randomly generated numbers
        System.out.print("\nEnter phone type: ");
        String userPhoneType = sc.nextLine();
        
        //if...else
        System.out.println("\nif...else");
        System.out.print("Phone type: ");
        if (userPhoneType.equals("W") || userPhoneType.equals("w")) {
            System.out.println("work");
        } else if (userPhoneType.equals("C") || userPhoneType.equals("c")) {
            System.out.println("cell");
        } else if (userPhoneType.equals("H") || userPhoneType.equals("h")) {
            System.out.println("home");
        } else if (userPhoneType.equals("N") || userPhoneType.equals("n")) {
            System.out.println("none");
        } else {
            System.out.println("Incorrect character entry.");
        }

        //switch
        System.out.println("\nswitch");
        System.out.print("Phone type: ");
        switch (userPhoneType) {
            case "W":
                System.out.println("work");
                break;
            case "w":
                System.out.println("work");
                break;
            case "C":
                System.out.println("cell");
                break;
            case "c":
                System.out.println("cell");
                break;
            case "H":
                System.out.println("home");
                break;
            case "h":
                System.out.println("home");
                break;
            case "N":
                System.out.println("none");
                break;
            case "n":
                System.out.println("none");
                break;
            default:
                System.out.println("Incorrect character entry.");
                break;
        }

    }
}