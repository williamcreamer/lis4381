import java.util.Scanner;
import java.util.Random; 

public class Methods
{
    public static void getRequirements() 
    {
        //Program introduction
        System.out.println("Developer: William Creamer\n");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void methods displays program requirements.");
        System.out.println("2) getUserInput(): Void methods prompts for user input,");
        System.out.println("\tthen calls two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3) myVoidMethod():");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Prints user's first name and age.");
        System.out.println("1) myValueReturningMethod():");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Returns String containing first name and age.");
    }
    
    public static void getUserInput()
    {
        //declare variables
        Scanner sc = new Scanner(System.in);
        String userName = "";
        String string = "";
        int userAge = 0;

        System.out.print("Enter first name: ");
        userName = sc.next();

        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        System.out.print("void method call: ");
        myVoidMethod(userName, userAge);

        System.out.print("value-returning method call: ");
        string = myValueReturningMethod(userName, userAge);
        System.out.println(string);
    }
    
    public static void myVoidMethod(String userName, int userAge)
    {
        System.out.println(userName + " is " + userAge + " years old.");
    }

    public static String myValueReturningMethod(String userName, int userAge)
    {
        return userName + " is " + userAge + " years old.";
    }
}