# LIS 4381 - Mobile Web Application Development

## William Creamer

### Project 1 Requirements:

*Six Parts:*

1. Develop a business card application that:
    * a) Displays a launcher icon image in both activities (screens).
    * b) Adds background color(s) to both activities.
    * c) Adds a border around the image and button.
    * d) Adds text shadow (button).
2. Skill Set 7: Pseudo-Random Number Generator Data Validation
    * Skill Set 7 link: [Skill Set 7](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/7_PsuedoRandomNumberGeneratorDataValidation "Skill Set 7")
3. Skill Set 8: Largest of Three Numbers
    * Skill Set 8 link: [Skill Set 8](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/8_LargestOfThreeNumbers "Skill Set 8")
4. Skill Set 9: Array Runtime Data Validation
    * Skill Set 9 link: [Skill Set 9](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/9_ArrayRuntimeDataValidation "Skill Set 9")
5. Questions
6. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381

#### README.md file should include the following items:

* Screenshot of running Android Studio - My Business Card, first user interface;
* Screenshot of running Android Studio - My Business Card, second user interface;

#### Assignment Screenshots:

|*Screenshot of Android Studio - My Business Card, first user interface*|*Screenshot of Android Studio - My Business Card, second user interface*|
|:---:|:---:|
| ![My Business Card Screenshot 1](img/myBusinessCard1.png) | ![My Business Card Screenshot 2](img/myBusinessCard2.png) |

***Screenshot of Pseudo-Random Number Generator Data Validation program running:***

![Pseudo-Random Number Generator Data Validation program screenshot](img/psuedoRandomNumberGeneratorDataValidation.png)

***Screenshot of Largest of Three Numbers program running:***

![Largest of Three Numbers program screenshot](img/largestOfThreeNumbers.png)

***Screenshot of Array Runtime Data Validation program running:***

![Array Runtime Data Validation program screenshot](img/arrayRuntimeDataValidation.png)

#### Related Links:

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")