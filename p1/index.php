<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1 web page.">
		<meta name="author" content="William Creamer">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>
		
		<style type="text/css">
		h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: </strong>Developed a business card application that has an image and button with borders around each, a launcher icon, text shadow, and unique background colors on both pages. Completed Skill Sets 7, 8, and 9.
				</p>
			
				<h4>Business Card Application (1/2)</h4>
				<img src="img/myBusinessCard1.png" class="img-responsive center-block" alt="Business Card Application (1/2)">

				<h4>Business Card Application (2/2)</h4>
				<img src="img/myBusinessCard2.png" class="img-responsive center-block" alt="Business Card Application (2/2)">

				<h4>Skill Set 7: Psuedo-Random Number Generator Data Validation Program</h4>
				<img src="img/psuedoRandomNumberGeneratorDataValidation.png" class="img-responsive center-block" alt="Psuedo-Random Number Generator Data Validation Program">
				
				<h4>Skill Set 8: Largest of Three Numbers Program</h4>
				<img src="img/largestOfThreeNumbers.png" class="img-responsive center-block" alt="Largest of Three Numbers Program">

				<h4>Skill Set 9: Array Runtime Data Validation Program</h4>
				<img src="img/arrayRuntimeDataValidation.png" class="img-responsive center-block" alt="Array Runtime Data Validation Program">

				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
