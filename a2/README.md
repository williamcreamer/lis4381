# LIS 4381 - Mobile Web Application Development

## William Creamer

### Assignment 2 Requirements:

*Six parts:*

1. Develop an application that:
    * a) Includes instrucions for a recipe.
    * b) Includes a photo of the dish.
    * c) Create a button.
    * d) Change the background color.
    * e) Change the text color.
2. Skill Set 1: Even or Odd
    * Skill Set 1 link: [Skill Set 1](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/1_EvenOrOdd "Skill Set 1")
3. Skill Set 2: Largest Number
    * Skill Set 2 link: [Skill Set 2](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/2_LargestNumber "Skill Set 2")
4. Skill Set 3: Arrays and Loops
    * Skill Set 3 link: [Skill Set 3](https://bitbucket.org/williamcreamer/lis4381/src/master/skillsets/3_ArraysAndLoops "Skill Set 3")
5. Questions
6. Bitbucket Repository Link
    * https://bitbucket.org/williamcreamer/lis4381

#### README.md file should include the following items:

* Screenshot of the recipe application running
* Screenshot of Even or Odd program running
* Screenshot of Largest Number program running
* Screenshot of Arrays and Loops program running

#### Assignment Screenshots:

|*Screenshot of Recipe Application's main page*|*Screenshot of Recipe Application's instructions page*|
|:---:|:---:|
| ![Recipe Application Main Page screenshot](img/recipeAppMainPage.png) | ![Recipe Application Instruction Page screenshot](img/recipeAppInstructionsPage.png) |

***Screenshot of Even or Odd program running:***

![Even or Odd program screenshot](img/evenOrOdd.png)

***Screenshot of Largest Number program running:***

![Largest Number program screenshot](img/largestNumber.png)

***Screenshot of Arrays and Loops program running:***

![Arrays and Loops program screenshot](img/arraysAndLoops.png)

#### Related Links: 

* Link to main README:
[Main README Link](https://bitbucket.org/williamcreamer/lis4381/ "Main README")