<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 2 web page.">
		<meta name="author" content="William Creamer">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>
		
		<style type="text/css">
		h1 
		{
			margin: 0;
			color: #7f3741;
			padding-top: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;
			text-shadow: 3px 3px #d9bba3
		}
		</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: </strong>Developed an application with multiple pages that includes instructions for a recipe, a photo of the dish, a button element, and unique background and text colors. Completed Skill Sets 1, 2, and 3.
				</p>

				<h4>Recipe Application main page</h4>
				<img src="img/recipeAppMainPage.png" class="img-responsive center-block" alt="Recipe Application Main Page">

				<h4>Recipe Application instructions page</h4>
				<img src="img/recipeAppInstructionsPage.png" class="img-responsive center-block" alt="Recipe Application Instructions Page">

				<h4>Skill Set 1: Even or Odd Program</h4>
				<img src="img/evenOrOdd.png" class="img-responsive center-block" alt="Even or Odd Program">
				
				<h4>Skill Set 2: Largest Number Program</h4>
				<img src="img/largestNumber.png" class="img-responsive center-block" alt="Largest Number Program">

				<h4>Skill Set 3: Arrays and Loops Program</h4>
				<img src="img/arraysAndLoops.png" class="img-responsive center-block" alt="Arrays and Loops Program">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
